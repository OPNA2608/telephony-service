Source: lomiri-telephony-service
Section: libs
Priority: optional
Maintainer: UBports developers <devs@ubports.com>
Build-Depends: cmake,
               dbus-test-runner,
               dconf-cli,
               debhelper-compat (= 12),
               dh-migrations | hello,
               gnome-keyring,
               liblomirihistoryservice-dev,
               libicu-dev,
               libmessaging-menu-dev,
               libnotify-dev,
               libphonenumber-dev,
               libtelepathy-qt5-dev,
               libprotobuf-dev,
               libpulse-dev,
               liblomiri-url-dispatcher-dev,
               pkg-config,
               python3,
               qml-module-qttest,
               qtbase5-dev (>= 5.0),
               qtdeclarative5-dev (>= 5.0),
               qtdeclarative5-dev-tools (>= 5.0),
               qtfeedback5-dev,
               qml-module-lomiri-components,
               qml-module-lomiri-layouts,
               qml-module-lomiri-performancemetrics,
               qml-module-lomiri-test,
               qtmultimedia5-dev (>= 5.0),
               qtpim5-dev (>= 5),
               libusermetricsinput-dev,
               telepathy-mission-control-5,
               xauth <!nocheck>,
               xvfb <!nocheck>,
Standards-Version: 3.9.4
Homepage: https://gitlab.com/ubports/development/core/lomiri-telephony-service
Vcs-Git: https://gitlab.com/ubports/development/core/lomiri-telephony-service.git
Vcs-Browser: https://gitlab.com/ubports/development/core/lomiri-telephony-service
X-Ubuntu-Use-Langpack: yes

Package: lomiri-telephony-service
Architecture: any
Multi-Arch: foreign
Conflicts: phone-app
Replaces: phone-app,
          telephony-service (<< 0.6.0~a~)
Breaks: telephony-service (<< 0.6.0~a~)
Provides: telephony-service (= ${binary:Version})
Depends: lomiri-schemas,
         history-service,
         libnotify4,
         telepathy-mission-control-5,
         lomiri-sounds,
         tone-generator[!s390x],
         libusermetricsinput1,
         dconf-cli,
         python3,
         python3-dbus,
         python3-gi,
         ${misc:Depends},
         ${shlibs:Depends},
Suggests: telepathy-ofono
Description: Telephony service components for Lomiri - backend
 Telephony service components for Lomiri.
 .
 This package contains the backend components providing the telephony 
 features such as calling and texting.

Package: telephony-service
Depends: lomiri-telephony-service, ${misc:Depends}
Architecture: all
Priority: optional
Section: oldlibs
Description: transitional package
 This is a transitional package. It can safely be removed.

Package: qml-module-lomiri-telephony
Architecture: any
Multi-Arch: same
Depends: lomiri-schemas,
         qtcontact5-galera[!s390x],
         lomiri-telephony-service (>= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends},
Suggests: telepathy-ofono
Breaks: qml-module-lomiri-telephony0.1 (<< 0.6.0~a~),
Replaces: qml-module-lomiri-telephony0.1 (<< 0.6.0~a~),
Description: Telephony service components for Lomiri - QML plugin
 Telephony service components for Lomiri.
 .
 This package contains the QML plugin providing the features from the telephony
 service to applications.

Package: qml-module-lomiri-telephony0.1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         qml-module-lomiri-telephony (= ${binary:Version}),
Description: Telephony service components for Lomiri - QML plugin (transitional package)
 Telephony service components for Lomiri.
 .
 This package contains the QML plugin providing the features from the telephony
 service to applications.
 .
 Transitional package.

Package: qml-module-lomiri-telephony-phonenumber
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         qml-module-lomiri-components
Breaks: qml-module-lomiri-telephony-phonenumber0.1 (<< 0.6.0~a~),
Replaces: qml-module-lomiri-telephony-phonenumber0.1 (<< 0.6.0~a~),
Description: Telephony PhoneNumber components for Lomiri - QML plugin
 Telephony PhoneNumber components for Lomiri.
 .
 This package contains the QML plugin providing the features from the telephony
 PhoneNumber to applications.

Package: qml-module-lomiri-telephony-phonenumber0.1
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
         ${shlibs:Depends},
         qml-module-lomiri-telephony-phonenumber (= ${binary:Version}),
Description: Telephony PhoneNumber components for Lomiri - QML plugin (transitional package)
 Telephony PhoneNumber components for Lomiri.
 .
 This package contains the QML plugin providing the features from the telephony
 PhoneNumber to applications.
 .
 Transitional package.
